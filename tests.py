from unittest import TestCase
from ast import super_split, Parserx, ArgNode, ProcessNode, ExpressionNode, CommandNode, IfNode


class TestSuper_split(TestCase):
    def test_super_split(self):
        test1 = "aaa(aa bb,c) a+a(-)"
        test2 = "x + x+2"
        test3 = "START PROCESS P(global r, x, global z)"
        self.assertEqual(['aaa', '(', 'aa', 'bb', ',', 'c', ')', 'a', '+', 'a', '(', '-', ')'], super_split(test1))
        self.assertEqual(['x', '+', 'x', '+', '2'], super_split(test2))
        self.assertEqual(['START', 'PROCESS', 'P', '(', 'global', 'r', ',', 'x', ',', 'global', 'z', ')'], super_split(test3))

class TestParserx(TestCase):
    def test_parse_argument(self):
        arg_tokens = ['global', 's']
        parser = Parserx(arg_tokens)
        arg_node = parser.parse_argument()
        self.assertEqual('s', arg_node.name)
        self.assertTrue(arg_node.global_value)
        self.assertEqual(2, parser.cur)

        arg_tokens = ['e']
        parser = Parserx(arg_tokens)
        arg_node = parser.parse_argument()
        self.assertEqual('e', arg_node.name)
        self.assertFalse(arg_node.global_value)
        self.assertEqual(1, parser.cur)

    def test_parse_process(self):
        process_tokens = ['START', 'PROCESS', 'P', '(', 'global', 'r', ')', 'END', 'PROCESS']
        parser = Parserx(process_tokens)
        process_node = parser.parse_process()
        correct_arg = ArgNode('r', True)
        self.assertIn(correct_arg, process_node.args)
        self.assertEqual(1, len(process_node.args))
        self.assertEqual('P', process_node.name)
        self.assertEqual(9, parser.cur)

        process_tokens = ['START', 'PROCESS', 'Precess', '(', 'global', 'r', ',', 'j', ',', 'global', 'opa', ')', 'END', 'PROCESS']
        parser = Parserx(process_tokens)
        process_node = parser.parse_process()
        correct_arg1 = ArgNode('r', True)
        correct_arg2 = ArgNode('j', False)
        correct_arg3 = ArgNode('opa', True)
        print(process_node.name, process_node.args)
        self.assertIn(correct_arg1, process_node.args)
        self.assertIn(correct_arg2, process_node.args)
        self.assertIn(correct_arg3, process_node.args)
        self.assertEqual(3, len(process_node.args))
        self.assertEqual('Precess', process_node.name)
        self.assertEqual(14, parser.cur)


    def test_parse_transaction(self):
        trans_tokens = []

    def test_parse_command(self):
        pass

    def test_parse_if_block(self):
        pass

    def test_parse_else_block(self):
        pass

    def test_parse_condition(self):
        pass

    def test_parse_expression(self):
        tokens = ['a','+','s','-', '4', 'then']
        parser = Parserx(tokens)
        expression_node = parser.parse_expression()
        self.assertEqual('4', expression_node.right)
        self.assertEqual('-', expression_node.operator)
        correct_left_node = ExpressionNode('a', '+', 's')
        self.assertEqual(correct_left_node, expression_node.left)
        self.assertEqual(5, parser.cur)

        tokens = ['94']
        parser = Parserx(tokens)
        expression_node = parser.parse_expression()
        self.assertEqual('94', expression_node)
        self.assertEqual(str, type(expression_node))
        self.assertEqual(1, parser.cur)

    def test_parse_read(self):
        tokens = ['read', '(', 'abc', ',', 'e', ')']
        parser = Parserx(tokens)
        command_node = CommandNode('1')
        parser.parse_read(command_node)
        self.assertEqual('read', command_node.ctype)
        self.assertEqual('abc', command_node.out)
        self.assertEqual('e', command_node.expression)
        self.assertEqual(6, parser.cur)

    def test_parse_write(self):
        tokens = ['write', '(', 'x', '+', '2', ',', 'a', ')']
        parser = Parserx(tokens)
        command_node = CommandNode('1')
        parser.parse_write(command_node)

        self.assertEqual('write', command_node.ctype)
        self.assertEqual('a', command_node.out)
        self.assertEqual('x', command_node.expression.left)
        self.assertEqual('+', command_node.expression.operator)
        self.assertEqual('2', command_node.expression.right)
        self.assertEqual(8, parser.cur)

    def test_parse_assign(self):
        tokens = ['x', '=', 'a', '-', 'b', '+', '1']
        parser = Parserx(tokens)
        command_node = CommandNode('1')
        parser.parse_assign(command_node)
        self.assertEqual('x', command_node.out)
        self.assertEqual('1', command_node.expression.right)
        self.assertEqual('+', command_node.expression.operator)
        correct_left_node = ExpressionNode('a', '-', 'b')
        self.assertEqual(correct_left_node, command_node.expression.left)
        self.assertEqual(7, parser.cur)
