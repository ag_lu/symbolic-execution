from z3 import *
from ast import *
from typing import List, Dict

'''
push pop add check model
'''


def create_symbolic(s):
    d = SymbolicMath()
    d.dict[s] = 1

    return d


def add_constant(c):
    d = SymbolicMath()
    d.coef = c

    return d


class SymbolicMath:
    def __init__(self):
        self.dict = {}
        self.coef = 0

    def __add__(self, other):
        nd = SymbolicMath()
        nd.coef = self.coef + other.coef
        nd.dict = self.dict.copy()
        for d in other.dict:
            if d in nd.dict:
                if nd.dict[d] + other.dict[d] != 0:
                    nd.dict[d] += other.dict[d]
                else:
                    del nd.dict[d]
            else:
                nd.dict[d] = other.dict[d]

        return nd

    def __sub__(self, other):
        nd = SymbolicMath()
        nd.coef = self.coef - other.coef
        nd.dict = self.dict.copy()
        for d in other.dict:
            if d in nd.dict:
                if nd.dict[d] - other.dict[d] != 0:
                    nd.dict[d] -= other.dict[d]
                else:
                    del nd.dict[d]
            else:
                nd.dict[d] = -other.dict[d]

        return nd

    def __str__(self):
        res = ''
        for d in self.dict:
            if res != '':
                res += ' '
            res += str(self.dict[d]) + d
        if self.coef != 0:
            if res != '':
                res += ' '
            res += str(self.coef)

        return res

    def __repr__(self):
        return self.__str__()

    def clone(self):
        nd = SymbolicMath()
        nd.coef = self.coef
        nd.dict = self.dict.copy()

        return nd

class ProcessInstance:
    def __init__(self, name):
        self.name = name
        self.transactions = []

    def add_transaction(self, transaction):
        self.transactions.append(transaction)


class TransactionExe:
    def __init__(self, name, path):
        self.name = name
        self.path = path

    def __str__(self):
        return str(self.name) + '(' + str(self.path) + ')'

    def __repr__(self):
        return self.__str__()


#path list
class PathController:
    def __init__(self, path):
        self.path = path
        self.cur = 0

    def next(self):
        if self.cur + 1 >= len(self.path):
            return -1

        return self.path[self.cur+1]

    def current(self):
        if self.cur >= len(self.path):
            return -1

        return self.path[self.cur]

    def inc_counter(self):
        self.cur += 1


class SymbolicVar:
    def __init__(self, name):
        self.name = name
        self.number = 1
        self.value = z3.Int(name + '_1')
        self.math = create_symbolic(name)

    def inc_number(self):
        self.number += 1
        self.value = z3.Int(self.name + '_' + str(self.number))


class SymbolicExe:
    def __init__(self, AST: ProcessNode, path: List[ProcessInstance]):
        #self.symval: Dict[str, SymbolicVar] = {}
        self.global_val: Dict[str, SymbolicVar] = {}
        self.locals: Dict[str, SymbolicVar] = {}
        self.AST: ProcessNode = AST
        self.solver = Solver()
        self.path = path
        self.conditions = []

    def add_global(self, name, val):
        self.global_val[name] = val

    def reverse_sign(self, sign):
        if sign == '==':
            return '!='
        elif sign == '!=':
            return '=='
        elif sign == '>':
            return '<='
        elif sign == '<':
            return '>='
        elif sign == '>=':
            return '<'
        elif sign == '<=':
            return '>'
        else:
            raise Exception('Unknown sign')

    def add_condition_z3(self, left, op, right):
        if op == '==':
            self.solver.add(left == right)
        elif op == '!=':
            self.solver.add(left != right)
        elif op == '>':
            self.solver.add(left > right)
        elif op == '<':
            self.solver.add(left < right)
        elif op == '>=':
            self.solver.add(left >= right)
        elif op == '<=':
            self.solver.add(left <= right)
        else:
            raise Exception('Unknown sign')

    def add_condition_math(self, left, op, right):
        self.conditions.append(
            str(left) + ' ' + op + ' ' + str(right))

    def traverse(self, proc_name, ast_commands, path_c: PathController):
        for command in ast_commands:
            if command.line_number == path_c.current():
                if type(command) == IfNode:
                    self.traverse_if(proc_name, command, path_c)
                else:
                    if command.ctype == 'READ':
                        self.traverse_read(proc_name,
                                           command, path_c)
                    elif command.ctype == 'WRITE':
                        self.traverse_write(proc_name,
                                            command, path_c)
                    else:
                        self.traverse_assign(proc_name,
                                             command, path_c)

    def traverse_if(self, proc_name, command: IfNode, path_c: PathController):
        left_z3 = self.get_expression_z3(proc_name,
                                         command.condition.left)
        right_z3 = self.get_expression_z3(proc_name,
                                          command.condition.right)
        operator = command.condition.operator
        left_math = self.get_expression_math(proc_name,
                                             command.condition.left)
        right_math = self.get_expression_math(proc_name,
                                              command.condition.right)

        path_c.inc_counter()
        if command.else_line_number == path_c.current():
            # condition false
            operator = self.reverse_sign(operator)
            self.add_condition_z3(left_z3, operator, right_z3)
            self.add_condition_math(left_math, operator,
                                    right_math)
            path_c.inc_counter()
            self.traverse(proc_name, command.else_block, path_c)
        elif command.endif_line_number == path_c.current():
            # condition false
            operator = self.reverse_sign(operator)
            self.add_condition_z3(left_z3, operator, right_z3)
            self.add_condition_math(left_math, operator, right_math)
            path_c.inc_counter()
        else:
            # condition true
            self.add_condition_z3(left_z3, operator, right_z3)
            self.add_condition_math(left_math, operator, right_math)
            self.traverse(proc_name, command.if_block, path_c)

    def traverse_read(self, proc_name, command: CommandNode,
                      path_c: PathController):
        into = command.out
        source = command.expression
        into = proc_name + '_' + into
        if into in self.locals:
            self.locals[into].inc_number()
        else:
            self.locals[into] = SymbolicVar(into)

        self.locals[into].math = \
            self.global_val[source].math.clone()

        self.solver.add(self.locals[into].value ==
                        self.global_val[source].value)
        path_c.inc_counter()

    def traverse_write(self, proc_name, command: CommandNode, path_c: PathController):
        expr_z3 = self.get_expression_z3(proc_name,
                                         command.expression)
        expr_math = self.get_expression_math(proc_name,
                                             command.expression)
        into = command.out
        self.global_val[into].inc_number()
        self.solver.add(self.global_val[into].value == expr_z3)
        self.global_val[into].math = expr_math
        path_c.inc_counter()

    def traverse_assign(self, proc_name, command: CommandNode, path_c: PathController):
        expr_z3 = self.get_expression_z3(proc_name,
                                         command.expression)
        expr_math = self.get_expression_math(proc_name,
                                             command.expression)
        into = proc_name + '_' + command.out
        if into in self.locals:
            self.locals[into].inc_number()
        else:
            self.locals[into] = SymbolicVar(into)

        self.solver.add(self.locals[into].value == expr_z3)
        self.locals[into].math = expr_math
        path_c.inc_counter()

    def get_expression_z3(self, proc_name, expr):
        if type(expr) == str:
            try:
                return int(expr)
            except ValueError:
                local = proc_name + '_' + expr
                return self.locals[local].value
        else:
            left = self.get_expression_z3(proc_name, expr.left)
            right = self.get_expression_z3(proc_name, expr.right)
            if expr.operator == '+':
                return left + right
            else:
                return left - right

    def get_expression_math(self, proc_name, expr):
        if type(expr) == str:
            try:
                return add_constant(int(expr))
            except ValueError:
                local = proc_name + '_' + expr
                return self.locals[local].math.clone()
        else:
            left = self.get_expression_math(proc_name, expr.left)
            right = self.get_expression_math(proc_name, expr.right)
            if expr.operator == '+':
                return left + right
            else:
                return left - right

    def run(self, proc_name, transaction):
        for t in self.AST.transactions:
            if t.name == transaction.name:
                self.traverse(proc_name, t.commands,
                              PathController(transaction.path))
                return

        raise Exception('Specified transaction {} does not exist'
                        %(transaction.name))

    def execute_path(self):
        for p in self.path:
            for t in p.transactions:
                self.run(p.name, t)

    def gen_symbolic(self):
        proc_names = set()
        for p in self.path:
            proc_names.add(p.name)

        for arg in self.AST.args:
            if arg.global_value:
                self.global_val[arg.name] = SymbolicVar(arg.name)
            else:
                for proc in proc_names:
                    name = proc + '_' + arg.name
                    self.locals[name] = SymbolicVar(name)








