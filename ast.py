from typing import List

def super_split(s):
    res = []
    tmp = ""
    for c in s:
        if (c != ' ') and (c != '\n') and (c != '\t'):
            if (c in '(),+-'):
                if len(tmp) != 0:
                    res.append(tmp)
                    tmp = ''
                res.append(c)
            else:
                tmp += c
        elif len(tmp) != 0:
            res.append(tmp)
            tmp = ''

    if len(tmp) != 0:
        res.append(tmp)

    return res

def hoba(file_name):
    f = open(file_name, "r")
    tokens = []
    for line in f.readlines():
        tokens += super_split(line)

    print(tokens)

    return tokens


class ASTNode:
    pass


class ProcessNode(ASTNode):
    def __init__(self, name):
        self.name = name
        self.args: List[ArgNode] = []
        self.transactions = []

    def add_arg(self, arg):
        self.args.append(arg)

    def add_transaction(self, transaction):
        self.transactions.append(transaction)

class ArgNode(ASTNode):
    def __init__(self, name, global_value):
        self.name = name
        self.global_value = global_value

    def __eq__(self, other):
        return self.name == other.name and self.global_value == other.global_value

    def __str__(self):
        g = ''
        if self.global_value:
            g = 'global '
        return g + self.name

    def __repr__(self):
        return self.__str__()


class TransactionNode(ASTNode):
    def __init__(self, name):
        self.name = name
        self.commands = []

    def add_command(self, command):
        self.commands.append(command)


class CommandNode(ASTNode):
    def __init__(self, line_number):
        self.line_number = line_number
        self.expression = None
        self.ctype = ''
        self.out = None

    def set_command(self, ctype, out):
        self.ctype = ctype
        self.out = out #read, write

    def append_expression(self, expression):
        self.expression = expression

    def __str__(self):
        return self.ctype + ' ' + self.out + ' ' + str(self.expression)

    def __repr__(self):
        return self.__str__()


class IfNode(ASTNode):
    def __init__(self, line_number):
        self.line_number = line_number
        self.if_block = []
        self.else_block = []
        self.else_line_number = 0
        self.endif_line_number = 0
        self.condition = None #expression_node

    def add_if_block(self, c):
        self.if_block.append(c)

    def add_else_block(self, c):
        self.else_block.append(c)

    def add_condition(self, condition):
        self.condition = condition

    def __str__(self):
        return 'if ' + str(self.condition) + ' then\n' + str(self.if_block) + '\nelse\n' + str(self.else_block)

    def __repr__(self):
        return self.__str__()

class ExpressionNode(ASTNode):
    def __init__(self, left, operator, right):
        self.left = left
        self.operator = operator
        self.right = right

    def add_expression(self, left, operator, right):
        self.left = left
        self.operator = operator
        self.right = right

    def __eq__(self, other):
        if type(self) != type(other):
            return False
        return self.left == other.left and self.operator == other.operator and self.right == other.right

    def __str__(self):
        if self.operator == '':
            return str(self.left)
        return '(' + str(self.left) + ' ' + self.operator + ' ' + str(self.right) + ')'

    def __repr__(self):
        return self.__str__()


class Parserx:
    def __init__(self, tokens):
        self.tokens = tokens
        self.token_count = len(self.tokens)
        self.cur = 0

    def move(self):
        self.cur += 1
        return self.tokens[self.cur - 1]

    def look(self):
        if self.cur < self.token_count:
            return self.tokens[self.cur]
        return 'EOF'

    def next(self):
        if self.cur + 1 < self.token_count:
            return self.tokens[self.cur + 1]
        else:
            return 'EOF'

    def step(self, expected):
        tmp = self.move()
        self.check(tmp, expected)

    def is_sign(self, c):
        return c == '+' or c == '-'  # or self.is_comp_operator(c)

    def is_comp_operator(self, c):
        return c == '>' or c == '<' or c == '<=' or c == '>=' or c == '==' or c == '<>'

    def expression_end(self, cur, next):
        return (cur.isalnum() and next.isalnum()) or (cur.isalnum() and next == ',')

    @staticmethod
    def check(cur, expected):
        if cur != expected:
            raise Exception('Invalid structure, expected {} got {}'.format(expected, cur))

    def parse_process(self):
        self.step('START')
        self.step('PROCESS')
        name = self.move()
        process = ProcessNode(name)
        self.step('(')
        while self.look() != ')':
            if self.look() == ',':
                self.move()
            arg = self.parse_argument()
            process.add_arg(arg)
        self.step(')')

        while self.look() != 'END':
            transaction = self.parse_transaction()
            process.add_transaction(transaction)

        self.step('END')
        self.step('PROCESS')
        return process

    def parse_argument(self):
        is_global = False
        if self.look() == 'global':
            is_global = True
            self.move()
        name = self.move()

        return ArgNode(name, is_global)

    def parse_transaction(self):
        self.step('BEGIN')
        self.step('TRANSACTION')
        name = self.move()
        transaction = TransactionNode(name)
        while (self.look() != 'COMMIT'):
            command = self.parse_command()
            transaction.add_command(command)

        self.step('COMMIT')
        self.step('TRANSACTION')
        return transaction

    def parse_command(self):
        line_num = int(self.move())
        command = CommandNode(line_num)
        if self.look() == 'READ':
            self.parse_read(command)
        elif self.look() == 'WRITE':
            self.parse_write(command)
        elif self.look() == 'IF':
            if_statement = IfNode(line_num)
            self.parse_if_block(if_statement)
            return if_statement
        else:
            self.parse_assign(command)

        return command

    def parse_expression(self):
        expression = None
        left = self.move()
        while self.is_sign(self.look()):
            operator = self.move()
            right = self.move()
            if expression is None:
                # create new
                expression = ExpressionNode(left, operator, right)
            else:
                # create and add existing expression
                expression = ExpressionNode(expression, operator, right)

        if expression is None:
            return left
        return expression

    def parse_if_block(self, if_statement):
        self.step('IF')
        self.parse_condition(if_statement)

        self.step('THEN')
        while self.next() != 'ELSE' and self.next() != 'ENDIF':
            command = self.parse_command()
            if_statement.add_if_block(command)

        if self.next() == 'ELSE':
            if_statement.else_line_number = int(self.move())
            self.parse_else_block(if_statement)

        if_statement.endif_line_number = int(self.move())
        self.step('ENDIF')

    def parse_else_block(self, if_statement):
        self.step('ELSE')
        while self.next() != 'ENDIF':
            command = self.parse_command()
            if_statement.add_else_block(command)

    def parse_read(self, command):
        ctype = self.move()
        self.step('(')
        out = self.move()
        self.step(',')
        expression = self.move()
        command.set_command(ctype, out)
        command.append_expression(expression)
        self.step(')')

    def parse_write(self, command):
        ctype = self.move()
        self.step('(')
        while self.look() != ',':
            expression = self.parse_expression()
            command.append_expression(expression)
        self.move()
        command.set_command(ctype, self.move())
        self.step(')')

    def parse_assign(self, command):
        ctype = 'assign'
        out = self.move()
        self.step('=')
        expression = self.parse_expression()
        command.set_command(ctype, out)
        command.append_expression(expression)

    def parse_condition(self, if_statement):
        left = self.parse_expression()
        comp_op = self.move()
        assert(self.is_comp_operator(comp_op))
        right = self.parse_expression()
        expression = ExpressionNode(left, comp_op, right)
        if_statement.add_condition(expression)

def print_transaction(t: TransactionNode):
    print('transaction', t.name)
    for c in t.commands:
        print(c)

def print_ast(process: ProcessNode):
    print(process.name, process.args)
    for t in process.transactions:
        print_transaction(t)

def get_ast():
    tokens = hoba("src")
    parser = Parserx(tokens)
    process = parser.parse_process()
    print_ast(process)
    return process
