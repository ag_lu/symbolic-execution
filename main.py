#P1(T1>T2(3,5))=>P2(T1>T2(3,5))
#P1(T1)>P2(T1)=>P1(T2(3,4,5))>P2(T2(3,5))
from symbolic import SymbolicExe, ProcessInstance, TransactionExe
from ast import get_ast

path = []


def parse_process_inst(path):
    pass


# P1(T1)>P2(T1)=>P1(T2(3,4,5))>P2(T2(3, 4,5))
# P2(T1>T2(3,4,5))=>P1(T1>T2(3,4,5))
p1 = ProcessInstance('P2')
t1 = TransactionExe('T1', [2])
p1.add_transaction(t1)
path.append(p1)

p2 = ProcessInstance('P2')
t1 = TransactionExe('T2', [3, 4, 5])
p2.add_transaction(t1)
path.append(p2)

p1 = ProcessInstance('P1')
t2 = TransactionExe('T1', [2])
p1.add_transaction(t2)
path.append(p1)



p2 = ProcessInstance('P1')
t2 = TransactionExe('T2', [3, 4, 5])
p2.add_transaction(t2)
path.append(p2)


print(path)
process = get_ast()
s = SymbolicExe(process, path)
s.gen_symbolic()
s.execute_path()
print(s.solver.check())
print(s.solver.assertions())
print(s.conditions)
print(s.global_val['r'].math)
print(s.solver.model())
print(s.global_val['r'].value)




'''
process = > "START PROCESS" < identifier > "(" < args > ")" < transactions > "END PROCESS"
args = > 𝜀 | < arg > "," < args >
arg = > < global > < identifier >
global = > 𝜀 | "global"

transactions = > 𝜀 | < transaction > < transactions >
transaction = > "BEGIN TRANSACTION" < identifier > < block > "COMMIT
TRANSACTION
" <identifier>
block = > 𝜀 | < command > < block >

command = > < number > < read > | < number > < write > | < number > < if > | < number >
< assign >
read = > "READ(" < identifier > "," < identifier > ")"
write = > "WRITE(" < expression > "," < identifier > ")"
if = > "IF" < condition > "THEN" < block > "ENDIF" | "IF" < condition > "THEN"
< block > "ELSE" < block > "ENDIF"
assign = > < identifier > "=" < expression >
expression = > < logical_expression > | < arithmetic_expression > 
logical_expression = > "TRUE" | "FALSE" | < condition >
arithmetic_expression = > < identifier > | identifier "+" < arithmetic_expression > |
identifier "-" < arithmetic_expression >
condition = > < expression > < compare > < expression >
compare = > ">=" | ">" | "<" | "<=" | "<>" | "="
number = > [0..9] | [0..9] < number >
identifier = > < char > | < identifier > < number > | < identifier > < char >
char = > ["a" - "z"] | ["A" - "Z"]
'''


'''
START PROCESS P(global r, a)
    BEGIN TRANSACTION T1
        1   read(x, r)
    COMMIT TRANSACTION
    
    BEGIN TRANSACTION T2
        2   if x > a then
        3       write(x - a, r)
        4   else 
        5       write(x + a, r) 
        6   endif 
    COMMIT TRANSACTION
END PROCESS
'''

'''
P1(T1>T2(3,5))=>P2(T1>T2(3,5))	(v1>r)&(v2>r)	r
P1(T1>T2(3,4,5))=>P2(T1>T2(3,5))	(v1<=r)&(v2>r-v1)	r-v1
P1(T1>T2(3,5))=>P2(T1>T2(3,4,5))	(v1>r)&(v2<=r)	r-v2
P1(T1>T2(3,4,5))=>P2(T1>T2(3,4,5))	(v1<=r)&(v2<=r-v1)	r-v1-v2
P2(T1>T2(3,5))=>P1(T1>T2(3,5))	(v1>r)&(v2>r)	r
P2(T1>T2(3,4,5))=>P1(T1>T2(3,5))	(v1>r-v2)&(v2<=r)	r-v2
P2(T1>T2(3,5))=>P1(T1>T2(3,4,5))	(v1<=r)&(v2>r)	r-v1
P2(T1>T2(3,4,5))=>P1(T1>T2(3,4,5))	(v1<=r-v2)&(v2<=r)	r-v1-v2
P1(T1)>P2(T1)=>P1(T2(3,5))>P2(T2(3,5))	(v1>r)&(v2>r)	r
P1(T1)>P2(T1)=>P1(T2(3,4,5))>P2(T2(3,5))	(v1<=r)&(v2>r)	r-v1
P1(T1)>P2(T1)=>P1(T2(3,4,5))>P2(T2(3,5))	(v1<=r)&(v2>r-v1)	r-v1
P1(T1)>P2(T1)=>P1(T2(3,5))>P2(T2(3, 4,5))	(v1>r)&(v2<=r)	r-v2
P1(T1)>P2(T1)=>P1(T2(3,4,5))>P2(T2(3, 4,5))	(v1<=r)&(v2<=r-v1)	r-v2
P1(T1)>P2(T1)=>P1(T2(3,4,5))>P2(T2(3, 4,5))	(v1<=r)&(v2<=r)	r-v2
P2(T1)>P1(T1)=>P2(T2(3,5))>P1(T2(3,5))	(v1>r)&(v2>r)	r
P2(T1)>P1(T1)=>P2(T2(3,4,5))>P1(T2(3,5))	(v1>r)&(v2<=r)	r-v2
P2(T1)>P1(T1)=>P2(T2(3,5))>P1(T2(3, 4,5))	(v1<=r)&(v2>r)	r-v1
P2(T1)>P1(T1)=>P2(T2(3,4,5))>P1(T2(3, 4,5))	(v2<=r)&(v1<=r-v2)	r-v1
P2(T1)>P1(T1)=>P2(T2(3,4,5))>P1(T2(3, 4,5))	(v2<=r)&(v1<=r)	r-v1

'''



